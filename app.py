from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from router import router


app = FastAPI(title="Code generation", version="0.0.1-dev")

app.include_router(router)
app.mount(
    "/static",
    StaticFiles(directory="static"),
    name="static",
)
