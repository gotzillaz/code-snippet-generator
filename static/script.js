function spawnBlockWithText(currentSnippetObject, isGenerate = false) {
  latestText =
    currentSnippetObject["title"] + " | " + currentSnippetObject["language"];

  // Create a new block element
  var block = document.createElement("li");
  block.classList.add("flex", "justify-between", "mb-4", "rounded");
  block.style.width = "366px"; // Set a fixed width for the block
  block.style.height = "40px";

  // Create a link element for the block
  var link = document.createElement("a");
  link.classList.add("w-full", "block", "p-2", "rounded");
  link.textContent = latestText;

  // Append the link to the block
  block.appendChild(link);

  // Prepend the block to the histBlock container
  histBlockContainer.insertBefore(block, histBlockContainer.firstChild);

  // Apply color to the spawned block
  if (isGenerate === true) {
    block.classList.add("bg-gray-500", "text-white", "clicked");
  } else {
    block.classList.add("bg-gray-300");
  }

  // Event listener for hover effect
  block.addEventListener("mouseenter", function () {
    // Apply lighter color when hovered over
    // block.classList.remove("bg-gray-500", "text-white");
    // block.classList.add("bg-gray-300");

    // Create a delete button for the block
    var deleteButton = document.createElement("button");
    deleteButton.classList.add(
      "bg-red-500",
      "text-white",
      "px-2",
      "py-1",
      "rounded",
      "slide-button"
    );
    deleteButton.textContent = "Delete";

    // Initially hide the delete button
    deleteButton.style.transform = "translateX(100%)";

    // Append the delete button to the block
    block.appendChild(deleteButton);

    // Slide in the delete button
    deleteButton.style.transform = "translateX(0)";

    // Event listener for delete button click
    deleteButton.addEventListener("click", function (event) {
      var allSnippetList = JSON.parse(localStorage.getItem("allSnippetList"));
      var index = -1;
      var loadedSnippetObject = JSON.parse(
        localStorage.getItem("currentSnippetObject")
      );
      if (currentSnippetObject.time == loadedSnippetObject.time) {
        initializePageState(false);
      }
      localStorage.removeItem(currentSnippetObject.time);
      for (let i = 0; i < allSnippetList.length; i++) {
        if (allSnippetList[i].time === currentSnippetObject.time) {
          index = i;
          break;
        }
      }
      allSnippetList.splice(index, 1);
      localStorage.setItem("allSnippetList", JSON.stringify(allSnippetList));

      event.stopPropagation(); // Prevent click event from bubbling to the block
      histBlockContainer.removeChild(block);
    });
  });

  block.addEventListener("mouseleave", function () {
    // Reapply dark color when not hovered over
    // block.classList.remove("bg-gray-300");
    // block.classList.add("bg-gray-500", "text-white");

    // Remove the delete button if exists
    var deleteButton = block.querySelector(".slide-button");
    if (deleteButton) {
      block.removeChild(deleteButton);
    }
  });

  // Load existing snippet from list
  block.addEventListener("click", function () {
    initializePageState(false);
    console.log("Current object", currentSnippetObject);

    const block = $(this);
    resetClickedButton();

    // Change the background color of the clicked block
    block.removeClass("bg-gray-300").addClass("bg-gray-500 text-white");

    // Update clickedBlock to the current block
    block.addClass("clicked");

    var loadedSnippetObject = JSON.parse(
      localStorage.getItem(currentSnippetObject.time)
    );
    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(loadedSnippetObject)
    );
    console.log("loading obj", loadedSnippetObject);

    // Enable DOMs based on the availibity of loaded snippet data
    if (loadedSnippetObject.code_prompt !== null) {
      $("#genCodeInput").val(loadedSnippetObject.code_prompt);
      $("#showCodeArea").empty();
      $("#showCodeArea").append(
        `<code class="${loadedSnippetObject.language}">${loadedSnippetObject.snippet}</code>`
      );

      $(
        "#showCodeArea, #codeFeedback, #improveCodeButton, #genTestButton"
      ).show();
      $("#genCodeInput, #genCodeButton").prop("disabled", true);
      $("#codeFeedback, #genTestButton").prop("disabled", false);
      $("#codeFeedback").on("input", function () {
        var value = $(this).val().trim();
        $("#improveCodeButton").prop("disabled", value === "");
      });
    }
    if (loadedSnippetObject.code_improve_prompt !== null) {
      $("#codeFeedback").val(loadedSnippetObject.code_improve_prompt);
      $("#improveCodeButton").prop("disabled", value === "");
    }
    if (loadedSnippetObject.test_snippet !== null) {
      $("#showTestArea").empty();
      $("#showTestArea").append(
        `<code class="${loadedSnippetObject.language}">${loadedSnippetObject.test_snippet}</code>`
      );
      $("#showTestArea, #genTestButton").prop("disabled", true);
      $("#testFeedback").prop("disabled", false);
      $("#showTestArea, #testFeedback, #improveTestButton").show();
      if (loadedSnippetObject.language === "python") {
        $("#runTestButton").prop("disabled", false);
        $("#runTestButton").show();
      }
    }
    if (loadedSnippetObject.test_improve_prompt !== null) {
      $("#testFeedback").val(loadedSnippetObject.test_improve_prompt);
      $("#improveTestButton").prop("disabled", value === "");
    }
    hljs.highlightAll();
    $("button:disabled").addClass("cursor-not-allowed");
    $("button:not(:disabled)").removeClass("cursor-not-allowed");
  });
}

function resetClickedButton() {
  const clickedBlock = $(".clicked"); // Retrieve previously clicked block, if any

  // Reset the style of the previously clicked block
  if (clickedBlock.length > 0) {
    clickedBlock.removeClass("bg-gray-500 text-white").addClass("bg-gray-300");
  }
}

function generateCode() {
  var deferred = $.Deferred();

  var input_data = {
    prompt: $("#genCodeInput").val(),
    messages: [],
    snippet: null,
    test_snippet: null,
    language: null,
  };
  console.log(input_data);
  $.post({
    url: "/generate_code",
    contentType: "application/json",
    data: JSON.stringify(input_data),
  }).done(function (data) {
    console.log(data);
    $("#showCodeArea").empty();
    $("#showCodeArea").append(
      `<code class="${data.result.language}">${data.result.snippet}</code>`
    );

    var currentSnippetObject = {
      title: data.result.title,
      time: Date.now(),
      messages: data.messages,
      language: data.result.language,
      code_prompt: $("#genCodeInput").val(),
      snippet: data.result.snippet,
      code_improve_prompt: null,
      test_snippet: null,
      test_improve_prompt: null,
      execution_result: null,
    };

    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(currentSnippetObject)
    );
    localStorage.setItem(
      currentSnippetObject.time,
      JSON.stringify(currentSnippetObject)
    );

    console.log(localStorage);

    hljs.highlightAll();
    deferred.resolve(currentSnippetObject);
  });
  return deferred.promise();
}

function improveCode() {
  var deferred = $.Deferred();
  var currentSnippetObject = JSON.parse(
    localStorage.getItem("currentSnippetObject")
  );
  var input_data = {
    prompt: $("#codeFeedback").val(),
    messages: currentSnippetObject.messages,
    snippet: currentSnippetObject.snippet,
    test_snippet: null,
    language: currentSnippetObject.language,
  };
  console.log(input_data);
  $.post({
    url: "/improve_code",
    contentType: "application/json",
    data: JSON.stringify(input_data),
  }).done(function (data) {
    console.log(data);
    $("#showCodeArea").empty();
    $("#showCodeArea").append(
      `<code class="${data.result.language}">${data.result.snippet}</code>`
    );

    currentSnippetObject.snippet = data.result.snippet;
    currentSnippetObject.code_improve_prompt = $("#codeFeedback").val();
    currentSnippetObject.messages = data.messages;

    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(currentSnippetObject)
    );
    localStorage.setItem(
      currentSnippetObject.time,
      JSON.stringify(currentSnippetObject)
    );

    console.log(localStorage);

    hljs.highlightAll();
    deferred.resolve();
  });
  return deferred.promise();
}

function generateTest() {
  var deferred = $.Deferred();
  var currentSnippetObject = JSON.parse(
    localStorage.getItem("currentSnippetObject")
  );
  var input_data = {
    prompt: null,
    messages: currentSnippetObject.messages,
    snippet: currentSnippetObject.snippet,
    test_snippet: null,
    language: currentSnippetObject.language,
  };
  console.log(input_data);
  $.post({
    url: "/generate_test",
    contentType: "application/json",
    data: JSON.stringify(input_data),
  }).done(function (data) {
    console.log(data);
    $("#showTestArea").empty();
    $("#showTestArea").append(
      `<code class="${data.result.language}">${data.result.snippet}</code>`
    );

    currentSnippetObject.test_snippet = data.result.snippet;
    currentSnippetObject.messages = data.messages;

    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(currentSnippetObject)
    );
    localStorage.setItem(
      currentSnippetObject.time,
      JSON.stringify(currentSnippetObject)
    );

    console.log(localStorage);

    hljs.highlightAll();
    deferred.resolve(currentSnippetObject);
  });
  return deferred.promise();
}

function improveTest() {
  var deferred = $.Deferred();
  var currentSnippetObject = JSON.parse(
    localStorage.getItem("currentSnippetObject")
  );
  var input_data = {
    prompt: $("#testFeedback").val(),
    messages: currentSnippetObject.messages,
    snippet: currentSnippetObject.snippet,
    test_snippet: currentSnippetObject.test_snippet,
    language: currentSnippetObject.language,
  };
  console.log(input_data);
  $.post({
    url: "/improve_test",
    contentType: "application/json",
    data: JSON.stringify(input_data),
  }).done(function (data) {
    console.log(data);
    $("#showTestArea").empty();
    $("#showTestArea").append(
      `<code class="${data.result.language}">${data.result.snippet}</code>`
    );

    currentSnippetObject.test_snippet = data.result.snippet;
    currentSnippetObject.test_improve_prompt = $("#testFeedback").val();
    currentSnippetObject.messages = data.messages;

    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(currentSnippetObject)
    );
    localStorage.setItem(
      currentSnippetObject.time,
      JSON.stringify(currentSnippetObject)
    );

    console.log(localStorage);

    hljs.highlightAll();
    deferred.resolve();
  });
  return deferred.promise();
}

function executeTest() {
  var deferred = $.Deferred();
  var currentSnippetObject = JSON.parse(
    localStorage.getItem("currentSnippetObject")
  );
  var input_data = {
    prompt: null,
    messages: currentSnippetObject.messages,
    snippet: currentSnippetObject.snippet,
    test_snippet: currentSnippetObject.test_snippet,
    language: currentSnippetObject.language,
  };
  console.log(input_data);
  $.post({
    url: "/execute_test",
    contentType: "application/json",
    data: JSON.stringify(input_data),
  }).done(function (data) {
    console.log(data);

    currentSnippetObject.execution_result = data.result;
    currentSnippetObject.messages = data.messages;

    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(currentSnippetObject)
    );
    localStorage.setItem(
      currentSnippetObject.time,
      JSON.stringify(currentSnippetObject)
    );

    console.log(localStorage);

    hljs.highlightAll();
    deferred.resolve(data);
  });
  return deferred.promise();
}

function fixError() {
  var deferred = $.Deferred();
  var currentSnippetObject = JSON.parse(
    localStorage.getItem("currentSnippetObject")
  );
  var input_data = {
    prompt: currentSnippetObject.execution_result,
    messages: currentSnippetObject.messages,
    snippet: currentSnippetObject.snippet,
    test_snippet: currentSnippetObject.test_snippet,
    language: currentSnippetObject.language,
  };
  console.log(input_data);
  $.post({
    url: "/fix_error",
    contentType: "application/json",
    data: JSON.stringify(input_data),
  }).done(function (data) {
    console.log(data);

    $("#showCodeArea").empty();
    $("#showCodeArea").append(
      `<code class="${data.result.language}">${data.result.snippet}</code>`
    );

    currentSnippetObject.snippet = data.result.snippet;
    currentSnippetObject.messages = data.messages;

    localStorage.setItem(
      "currentSnippetObject",
      JSON.stringify(currentSnippetObject)
    );
    localStorage.setItem(
      currentSnippetObject.time,
      JSON.stringify(currentSnippetObject)
    );

    console.log(localStorage);

    hljs.highlightAll();
    deferred.resolve(data);
  });
  return deferred.promise();
}

// Function to initialize the page state
function initializePageState(isSpawn) {
  $(
    "#genCodeButton, #showCodeArea, #codeFeedback, #improveCodeButton, #genTestButton, #showTestArea, #testFeedback, #improveTestButton, #runTestButton, #executionMessage, #regenerateButton"
  ).prop("disabled", true);
  $(
    "#line, #showCodeArea, #codeFeedback, #improveCodeButton, #genTestButton, #showTestArea, #testFeedback, #improveTestButton, #runTestButton, #executionMessage, #regenerateButton"
  ).hide();
  $("#genCodeInput").val("");
  $("#showCodeArea").empty();
  $("#codeFeedback").val("");
  $("#showTestArea").empty();
  $("#testFeedback").val("");
  $("#genCodeInput").prop("disabled", false);
  $("button:disabled").addClass("cursor-not-allowed");
  if (isSpawn === true) {
    var allSnippetList =
      JSON.parse(localStorage.getItem("allSnippetList")) || [];
    for (let i = 0; i < allSnippetList.length; i++) {
      spawnBlockWithText(allSnippetList[i]);
    }
    localStorage.setItem("currentSnippetObject", "{}");
    var currentSnippet = -1;
    console.log("currentStatus:", allSnippetList, currentSnippet);
  }
}

$(document).ready(function () {
  // Initialize page state when the document is ready
  initializePageState(true);

  // Enable genCodeInput and genCodeButton when genCodeInput is filled
  $("#genCodeInput").on("input", function () {
    var value = $(this).val().trim();
    $("#genCodeButton").prop("disabled", value === "");
    $("button:not(:disabled)").removeClass("cursor-not-allowed");
  });

  // Handle click event for genCodeButton
  $("#genCodeButton").on("click", function () {
    // generateCode();
    // spawnBlockWithText();
    $("#progressGenCodeButton").show();
    generateCode().done(function (data) {
      resetClickedButton();
      var allSnippetList =
        JSON.parse(localStorage.getItem("allSnippetList")) || [];
      console.log("before push new snippet", allSnippetList);

      allSnippetList.push({
        time: data.time,
        title: data.title,
        language: data.language,
      });
      console.log("after push new snippet", allSnippetList);
      localStorage.setItem("allSnippetList", JSON.stringify(allSnippetList));

      spawnBlockWithText(data, (isGenerate = true));
      // Show showCodeArea, codeFeedback, and improveCodeButton
      $(
        "#showCodeArea, #codeFeedback, #improveCodeButton, #genTestButton"
      ).show();

      // Disable genCodeInput and genCodeButton
      $("#genCodeInput, #genCodeButton").prop("disabled", true);

      // Enable other elements
      $("#showCodeArea, #codeFeedback, #genTestButton").prop("disabled", false);

      // Enable improveCodeButton when codeFeedback is filled
      $("#codeFeedback").on("input", function () {
        var value = $(this).val().trim();
        $("#improveCodeButton").prop("disabled", value === "");
        $("button:not(:disabled)").removeClass("cursor-not-allowed");
      });
      $("#progressGenCodeButton").hide();
      $("button:disabled").addClass("cursor-not-allowed");
      $("button:not(:disabled)").removeClass("cursor-not-allowed");
    });
  });

  // Handle click event for improveCodeButton
  $("#improveCodeButton").on("click", function () {
    $("#progressImproveCodeButton").show();
    improveCode().done(function () {
      $("#progressImproveCodeButton").hide();
    });
  });

  // Handle click event for genTestButton
  $("#genTestButton").on("click", function () {
    $("#progressGenTestButton").show();
    generateTest().done(function (data) {
      $("#progressGenTestButton").hide();
      // Show showTestArea, testFeedback, and improveTestButton
      $("#showTestArea, #testFeedback, #improveTestButton").show();

      // Disable showCodeArea and getTestButton
      $("#showCodeArea, #genTestButton").prop("disabled", true);

      // Enable testFeedback
      $("#testFeedback").prop("disabled", false);

      // Show runTestButton only for python code
      if (data.language === "python") {
        $("#runTestButton").show();
        $("#runTestButton").prop("disabled", false);
      }

      $("button:disabled").addClass("cursor-not-allowed");
      $("button:not(:disabled)").removeClass("cursor-not-allowed");

      // Enable improveCodeButton when codeFeedback is filled
      $("#testFeedback").on("input", function () {
        var value = $(this).val().trim();
        $("#improveTestButton").prop("disabled", value === "");
        $("button:not(:disabled)").removeClass("cursor-not-allowed");
      });
    });
  });

  // Handle click event for improveCodeButton
  $("#improveTestButton").on("click", function () {
    $("#progressImproveTestButton").show();
    improveTest().done(function () {
      $("#progressImproveTestButton").hide();
    });
  });

  // Handle click event for runTestButton
  $("#runTestButton").on("click", function () {
    $("#progressRunTestButton").show();
    $("#executionMessage, #regenerateButton").hide();
    executeTest().done(function (data) {
      $("#progressRunTestButton").hide();
      // Show success message or regnerateButton
      if (data.result.trim() === "Success") {
        $("#executionMessage").show();
      } else {
        $("#regenerateButton").show();
        $("#regenerateButton").prop("disabled", false);
      }
      $("button:disabled").addClass("cursor-not-allowed");
      $("button:not(:disabled)").removeClass("cursor-not-allowed");
    });
  });

  // Handle click event for regenerateButton
  $("#regenerateButton").on("click", function () {
    $("#progressRegenerateButton").show();
    fixError().done(function (data) {
      $("#progressRegenerateButton").hide();
      $("#regenerateButton").hide();
    });
  });

  // Handle click event for newSnippetButton
  $("#newSnippetButton").on("click", function () {
    // Reset all DOMs in the middle column.
    resetClickedButton();
    initializePageState(false);
  });
});
