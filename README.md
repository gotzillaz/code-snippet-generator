# Code Snippet Generation

This assignment evaluates your ability to write clean Python code and solve AI/LLM problems. Your main task is to implement the APIs necessary for generating code snippets.
The original discription is on [DESCRIPTION.md](DESCRIPTION.md).

## Getting Started

To work on this project, you need a basic understanding of FastAPI, Python, and Javascript. Implement the backend APIs using FastAPI. For frontend work, you have two options:

- Implement frontend logic by dividing the design page into multiple parts and implementing logic in the backend using FastAPI template(s).
- Write Javascript code to create a single-page app and interact with the backend using APIs. If not familiar with frontend work, use ChatGPT or another AI tool to assist.

The repository's basic settings are configured to run in a specific way. A Dockerfile installs requirements and runs `app.py`, so refrain from updating the app structure unless necessary.

Use OpenAI ChatGPT 3.5 turbo model for this project. Sign up on ChatGPT to obtain the API key. Development and testing should cost less than $0.1.

To start working on the project:
1. You will be provided with a zip file containing the project structure.
2. Set up a new Python 3.11 environment and install dependencies.
3. Create the .env file (see .env.example) and add the required environment variables. Update .env.example with correct variable names.
4. Understand the application logic and frontend workings by reviewing `design.html`.
5. Work on `design.html` and `app.py` to create a coherent system meeting assignment requirements. Create additional files if necessary.

To build and run the project:
```bash
bash start-docker-server.sh <your-docker-image-name>
```
The application will be served at `http://localhost:8000`.
Note that `Docker` must be installed.