def clean_snippet(snippet: str, is_code: bool = False):
    snippet_line = snippet.strip().strip("`").split("\n")
    if is_code:
        language, title = list(map(lambda w: w.strip(), snippet_line[0].split("|")))
    else:
        language = snippet_line[0].strip()
        title = None
    clean_snippet = "\n".join(snippet_line[1:]).strip()
    return {"snippet": clean_snippet, "language": language, "title": title}


assert clean_snippet(
    "```javascript\nfunction sum(a, b) {\n    return a + b;\n}\n```"
) == {
    "snippet": "function sum(a, b) {\n    return a + b;\n}",
    "language": "javascript",
    "title": None,
}

assert clean_snippet(
    "```javascript | Sum\nfunction sum(a, b) {\n    return a + b;\n}\n```", is_code=True
) == {
    "snippet": "function sum(a, b) {\n    return a + b;\n}",
    "language": "javascript",
    "title": "Sum",
}
