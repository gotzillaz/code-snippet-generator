from typing import List
import traceback

from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates
from openai import OpenAI
from pydantic import BaseModel

import utils
from settings import environment


openai_client = OpenAI(
    api_key=environment.OPENAI_API_KEY,
    organization=environment.OPENAI_ORGANIZATION_ID,
    project=environment.OPENAI_PROJECT_ID,
)

router = APIRouter()


class RequestModel(BaseModel):
    messages: List[dict] | None
    snippet: str | None
    test_snippet: str | None
    language: str | None
    prompt: str | None


@router.get("/")
async def load_app(request: Request):
    template = Jinja2Templates(directory="./")
    return template.TemplateResponse(request=request, name="design.html")


@router.post("/generate_code")
async def generate_code(request: RequestModel) -> dict:
    messages = [
        {
            "role": "system",
            "content": """You are a senior software engineer. \
                    Your task is to write a code snippet from the given question.""",
        },
        {
            "role": "user",
            "content": f"""
                Question: 'Generate a function of Python program for checking an even number.'\n
                Answer: ```python | Even Tester\n
                def is_even(n):\n
                    return n%2```\n
                ---\n
                Question: 'Generate a function of JavaScript program sum of two parameters.'\n
                Answer: ```javascript | Sum of Two Params\n
                function sum(a, b) {{\n
                    return a + b;\n
                }}```\n
                ---\n
                Question: 'Generate a function of {request.prompt}.'\n
                Answer:
                """,
        },
    ]
    completion = openai_client.chat.completions.create(
        model="gpt-3.5-turbo",
        seed=64,
        messages=messages,
    )
    messages.append(
        {
            "role": completion.choices[0].message.role,
            "content": completion.choices[0].message.content,
        }
    )
    snippet_obj = utils.clean_snippet(
        completion.choices[0].message.content, is_code=True
    )
    return {"type": "snippet", "result": snippet_obj, "messages": messages}


@router.post("/improve_code")
async def improve_code(request: RequestModel):
    messages = request.messages + [
        {
            "role": "user",
            "content": f"""
                Improve the code of {request.language} function: \n
                {request.snippet} \n
                by \n
                {request.prompt}
                """,
        }
    ]
    completion = openai_client.chat.completions.create(
        model="gpt-3.5-turbo",
        seed=64,
        messages=messages,
    )
    messages.append(
        {
            "role": completion.choices[0].message.role,
            "content": completion.choices[0].message.content,
        }
    )
    snippet_obj = utils.clean_snippet(completion.choices[0].message.content)
    return {"type": "snippet", "result": snippet_obj, "messages": messages}


@router.post("/generate_test")
async def generate_test(request: RequestModel):
    messages = request.messages + [
        {
            "role": "user",
            "content": f"""
                Question: 'Generate unit tests with assert function for Python program.'\n
                Snippet: ```python\n
                def is_even(n):\n
                    return n%2'\n```
                Answer: ```python\n
                assert is_even(1) == False\n
                assert is_even(2) == True```\n
                ---\n
                Question: 'Generate unit tests with assert function for JavaScript program.'\n
                Snippet: ```javascript\n
                function sum(a, b) {{\n
                    return a + b;\n
                }}```\n
                Answer: ```javascript\n
                console.assert(sum(5, 3) === 8);\n
                console.assert(sum(-2, 7) === 5);```\n
                ---\n
                Question: 'Generate testcases with assert function for {request.language} program.'\n
                Snippet: ```{request.language}\n{request.snippet}```\n
                Answer:
                """,
        },
    ]
    completion = openai_client.chat.completions.create(
        model="gpt-3.5-turbo",
        seed=64,
        messages=messages,
    )
    messages.append(
        {
            "role": completion.choices[0].message.role,
            "content": completion.choices[0].message.content,
        }
    )
    test_snippet_obj = utils.clean_snippet(completion.choices[0].message.content)
    return {"type": "test_snippet", "result": test_snippet_obj, "messages": messages}


@router.post("/improve_test")
async def improve_test(request: RequestModel):
    messages = request.messages + [
        {
            "role": "user",
            "content": f"""
                Improve the test cases of {request.language} function: \n
                {request.snippet} \n
                from \n
                {request.test_snippet} \n
                by \n
                {request.prompt}
                """,
        }
    ]
    completion = openai_client.chat.completions.create(
        model="gpt-3.5-turbo",
        seed=64,
        messages=messages,
    )
    messages.append(
        {
            "role": completion.choices[0].message.role,
            "content": completion.choices[0].message.content,
        }
    )
    test_snippet_obj = utils.clean_snippet(completion.choices[0].message.content)
    return {"type": "test_snippet", "result": test_snippet_obj, "messages": messages}


@router.post("/execute_test")
async def execute_test(request: RequestModel):
    test_expression = f"{request.snippet}\n{request.test_snippet}"
    result = "Success"
    try:
        exec(test_expression)
    except Exception as e:
        result = traceback.format_exc()
    return {
        "type": "execution_result",
        "result": result,
        "messages": request.messages,
    }


@router.post("/fix_error")
async def fix_error(request: RequestModel):
    messages = request.messages + [
        {
            "role": "user",
            "content": f"""
                Fix the error of {request.language} function:\n
                {request.snippet} \n
                The executed test cases: \n
                {request.test_snippet} \n
                and the error list: \n
                {request.prompt} \n
                Generate an error fix for the function: \n
                ```{request.language}
                """,
        }
    ]
    completion = openai_client.chat.completions.create(
        model="gpt-3.5-turbo",
        seed=64,
        messages=messages,
    )
    messages.append(
        {
            "role": completion.choices[0].message.role,
            "content": completion.choices[0].message.content,
        }
    )
    snippet_obj = utils.clean_snippet(completion.choices[0].message.content)
    return {"type": "snippet", "result": snippet_obj, "messages": messages}


@router.post("/execute_test_chatgpt")
async def execute_test_chatgpt(request: RequestModel):
    messages = request.messages + [
        {
            "role": "user",
            "content": f"""
                Can you verify if the {request.language} code is correct by answering 'Success' or 'Failure' for \n
                {request.snippet} \n
                {request.test_snippet}
                """,
        },
    ]
    completion = openai_client.chat.completions.create(
        model="gpt-3.5-turbo",
        seed=64,
        messages=messages,
    )
    return {
        "type": "generated_execution_result",
        "result": completion.choices[0].message.content,
        "messages": messages,
    }
