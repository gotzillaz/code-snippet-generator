from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    OPENAI_API_KEY: str
    OPENAI_ORGANIZATION_ID: str | None = None
    OPENAI_PROJECT_ID: str | None = None
    model_config = SettingsConfigDict(env_file=".env", extra="ignore")


environment = Settings()
